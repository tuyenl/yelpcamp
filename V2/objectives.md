# Add Mongoose
    * Install and configure Mongoose
    * Setup campground model
    * Use campground model inside of our routes

# Show Page
    * Review the RESTful routes
    * Add description to our campground model
    * Show db.collection.drop() // example: db.campgrounds.drop() removes collection from mongo db
    * Add a show route/template

# RESTFUL ROUTES

Name      url            Verb       Desc
======================================================================================
INDEX     /dogs          GET        Display a list of all dogs
NEW       /dogs/new      GET        Display form to make new dog
CREATE    /dogs          POST       Add new dog to DB
SHOW      /dogs/:id      GET        Show info about one dog