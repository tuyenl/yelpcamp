var express = require('express'),app = express(), 
bodyParser = require('body-parser'),
mongoose = require('mongoose');
mongoose.connect("mongodb://localhost:27017/yelp_camp",{ useNewUrlParser:true, useUnifiedTopology: true });
app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");

//SCHEMA SETUP
var campgroundSchema = new mongoose.Schema({
    name: String,
    image: String,
    description: String
});

var Campground = mongoose.model("Campground", campgroundSchema)
// Creating new campground
/* Campground.create(
    {
        name: "Mount Everest",
        image: "https://pixabay.com/get/57e1d14a4e52ae14f6da8c7dda793f7f1636dfe2564c704c7d2f7ddd904ec55b_340.jpg",
        description: "The tallest mountain, with the coldest temperatures, no human has ever climbed this mountain before"
    }, (err, campground)=>{
        if(err){
            console.log;
        }
        else{
            console.log("new created campground");
            console.log(campground);
        }
    }
) */


app.get("/", (req,res) => {
    res.render("landing");
})

//INDEX -- show all campgrounds
app.get("/campgrounds", (req, res) =>{
    //res.render("campgrounds",{campgrounds: campgrounds})
    //Retrieve all campground from db
    Campground.find({}, (err, allcampgrounds)=>{
        if(err){
            console.log(err);
        }
        else{
            res.render("index", {campgrounds: allcampgrounds});
        }
    })
})

//CREATE -- add new campground to database
app.post("/campgrounds", (req,res) => {
    var name = req.body.name;
    var image = req.body.image;
    var description = req.body.description;
    var newCampgrounds = {name: name,image: image, description: description};
    Campground.create(newCampgrounds,(err, newlyCreated)=>{
        if(err){
            console.log(err);
        }
        else{
            res.redirect("/campgrounds");
        }
    })
})



//NEW -- Show form to create new campground
app.get("/campgrounds/new", (req, res) =>{
    res.render("new");
})


//SHOW -- show more info on one campground
app.get("/campgrounds/:id", (req,res) =>{
    Campground.findById(req.params.id, (err, foundCampground)=>{
        if(err){
            console.log(err);
        }
        else{
            res.render("show", {campground: foundCampground})
        }
    })
})

app.listen(3000, () =>{
    console.log("Server has started for yelpcamp on port 3000");
})